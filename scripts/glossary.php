<?php
/**************************************************************
* INFO/CS 1300
* Fall 2016
* Megan Tice
**************************************************************/


// Read the CSV file in the data subdirectory into an array
function term() {
    $term = [];
    $term = array_map('str_getcsv',file('data/archery_terms.csv')); 
    $x = normalToAssoc($term);
    sortArray($x);
}

// Convert the normal array to an associative array, and sort the associative array by keys 
function normalToAssoc($norm_array) {
    $assoc = [];
    foreach($norm_array as $temp) {
        $assoc[$temp[0]]=$temp[1];
    }
    ksort($assoc);
    return $assoc;
}

// Create a dl list from the sorted array
function sortArray($AssocArray) {
    echo "<dl>";
    foreach($AssocArray as $key=>$value){
    echo "<dt>$key</dt><dd>$value</dd>";
    }
    echo "</dl>";
}
term();
?>